import pynq
from pynq import GPIO

__author__ = "Tan H. Nguyen, PhD"
__copyright__ = "Copyright 2020, PathAI"
__email__ = "tan.nguyen@pathai.com"

class tpd_pynqOverlay(pynq.Overlay):
    """Define a new class name tpd_pynOverlay"""
    def __init__(self, bitfile, **kwargs):
        super().__init__(bitfile, **kwargs)
        if self.is_loaded():
            pass